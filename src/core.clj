(ns core
  (:use [net.cgrand.enlive-html]
        [clojure.java.shell :only [sh]])
  (:import java.net.URL))

(defn partition-with
  [f coll]
  (lazy-seq
    (when-let [s (seq coll)]
      (let [run (cons (first s) (take-while (complement f) (next s)))]
        (cons run (partition-with f (seq (drop (count run) s))))))))

(defn ww[u] (prn u) (Thread/sleep 5000) (-> u URL. html-resource))
(defn ee[tt] (->> (select tt [:td#maincolumn]) first :content (partition-with #(-> % :attrs :class (= "podcast-title header2"))) rest))
(defn yyyf[yy] (->> yy (filter map?) (map :content) first first))
(defn yyyg[yy] (->> yy (filter map?) (map :content) (filter seq?) (apply concat) (map :attrs) (map :href) (filter #(re-matches #"http.*mp3" (str %))) first))
(defn yyyj[yy] (->> yy (filter map?) (map :content) (filter seq?) (take-last 2)))
(defn yyyj[yy] (->> yy (filter map?) (map :content) (filter seq?) (take-last 2) first))
(defn rrrr[yy] (->> yy (filter map?) (map :content) (filter seq?) (apply concat) (map :attrs) (map :href) (map #(re-find #"http.*episodes/(.*)" (str %))) (remove nil?) first last))
(defn uuu[yy] [(rrrr yy) (yyyf yy) (yyyg yy) (yyyj yy)])
(defn aa[b n] (->> n range (map #(str  b "/?p=" %))))

(defn step1[b n]
  (let [t1 (aa b n)
        t2 (map ww t1)
        t3 (map ee t2)]
    t3))

(defn step2[s]
  (->> s (mapcat #(map uuu %)) doall))


(comment
  (def a1 (step1 "url" 35))
  (def a2 (step2 a1))
  (spit "a.edn" (pr-str a))
  (->> a2 (map rest) (map rest) (map first) (remove nil?) (map #(println "wget " %)) dorun)
  )


